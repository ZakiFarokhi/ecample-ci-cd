"use strict";

var express = require('express');

var app = express();

var bodyParser = require('body-parser');

var cors = require('cors');

app.use(bodyParser.json());
app.use(cors());
app.listen(process.env.PORT || 2000, function () {
  console.log('connected');
});
app.get('/', function (req, res) {
  res.status(200).json({
    success: true
  });
});